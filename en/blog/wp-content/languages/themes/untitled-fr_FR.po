# Translation of Untitled in French (France)
# This file is distributed under the same license as the Untitled package.
msgid ""
msgstr ""
"PO-Revision-Date: 2016-09-01 08:16:10+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: GlotPress/2.1.0-alpha\n"
"Project-Id-Version: Untitled\n"

#. Description of the plugin/theme
msgid "A nice bold theme. Features full-bleed featured posts and featured images, a fixed header, and subtle CSS3 transitions."
msgstr "Un joli thème au charme particulier. Permet des articles et des images présentés en pleine page, un en-tête fixe, et de subtiles transitions en CSS3."

#. Theme Name of the plugin/theme
msgid "Untitled"
msgstr "Untitled"

#. Template Name of the plugin/theme
msgid "Full-Width Page"
msgstr "Page Pleine Largeur"

#: single.php:90
msgid "This entry was posted in %1$s. Bookmark the <a href=\"%3$s\" title=\"Permalink to %4$s\" rel=\"bookmark\">permalink</a>."
msgstr "Ce contenu a été publié dans %1$s. Vous pouvez le mettre en favoris avec <a href=\"%3$s\" title=\"Permalien pour %4$s\" rel=\"bookmark\">ce permalien</a>."

#: single.php:88
msgid "This entry was posted in %1$s and tagged %2$s. Bookmark the <a href=\"%3$s\" title=\"Permalink to %4$s\" rel=\"bookmark\">permalink</a>."
msgstr "Ce contenu a été publié dans %1$s, avec comme mot(s)-clé(s) %2$s. Vous pouvez le mettre en favoris avec <a href=\"%3$s\" title=\"Permalien pour %4$s\" rel=\"bookmark\">ce permalien</a>."

#: single.php:82
msgid "Bookmark the <a href=\"%3$s\" title=\"Permalink to %4$s\" rel=\"bookmark\">permalink</a>."
msgstr "Vous pouvez la mettre en favoris avec <a href=\"%3$s\" title=\"Permalien pour %4$s\" rel=\"bookmark\">ce permalien</a>."

#: single.php:80
msgid "This entry was tagged %2$s. Bookmark the <a href=\"%3$s\" title=\"Permalink to %4$s\" rel=\"bookmark\">permalink</a>."
msgstr "Cette entrée est taguée %2$s. Bookmarquez ce <a href=\"%3$s\" title=\"Permalien vers %4$s\" rel=\"bookmark\">permalien</a>."

#. translators: used between list items, there is a space after the comma
#: single.php:72 single.php:75 slider.php:27
msgid ", "
msgstr ", "

#: searchform.php:11
msgctxt "submit button"
msgid "Search"
msgstr "Rechercher"

#: searchform.php:9
msgctxt "assistive text"
msgid "Search"
msgstr "Rechercher"

#: searchform.php:10
msgctxt "placeholder"
msgid "Search &hellip;"
msgstr "Recherche&hellip;"

#: search.php:17
msgid "Search Results for: %s"
msgstr "Résultats de recherche pour %s"

#: no-results.php:28
msgid "It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help."
msgstr "Il semblerait que nous ne soyons pas en mesure de trouver votre contenu. Essayez en lançant une recherche."

#: no-results.php:23
msgid "Sorry, but nothing matched your search terms. Please try again with some different keywords."
msgstr "Désolé, mais rien ne correspond à votre recherche. Veuillez réessayer avec des mots différents."

#: no-results.php:19
msgid "Ready to publish your first post? <a href=\"%1$s\">Get started here</a>."
msgstr "Prêt à publier votre premier article&nbsp;? <a href=\"%1$s\">Lancez-vous</a>&nbsp;!"

#: no-results.php:13
msgid "Nothing Found"
msgstr "Rien de trouvé"

#: inc/template-tags.php:125
msgid "<a href=\"%1$s\" title=\"%2$s\" rel=\"bookmark\"><time class=\"entry-date\" datetime=\"%3$s\">%4$s</time></a><span class=\"byline\"> by <span class=\"author vcard\"><a class=\"url fn n\" href=\"%5$s\" title=\"%6$s\" rel=\"author\">%7$s</a></span></span>"
msgstr "<a href=\"%1$s\" title=\"%2$s\" rel=\"bookmark\"><time class=\"entry-date\" datetime=\"%3$s\">%4$s</time></a><span class=\"byline\"> par <span class=\"author vcard\"><a class=\"url fn n\" href=\"%5$s\" title=\"%6$s\" rel=\"author\">%7$s</a></span></span>"

#: inc/template-tags.php:131
msgid "View all posts by %s"
msgstr "Afficher tous les articles par %s"

#. translators: 1: date, 2: time
#: inc/template-tags.php:98
msgid "%1$s at %2$s"
msgstr "%1$s à %2$s"

#: inc/template-tags.php:90
msgid "Your comment is awaiting moderation."
msgstr "Votre commentaire est en attente de validation."

#: inc/template-tags.php:87
msgid "%s <span class=\"says\">says:</span>"
msgstr "%s <span class=\"says\">dit&nbsp;:</span>"

#: inc/template-tags.php:77
msgid "Pingback:"
msgstr "Ping&nbsp;:"

#: inc/template-tags.php:52
msgid "Newer posts <span class=\"meta-nav\">&rarr;</span>"
msgstr "Articles plus récents <span class=\"meta-nav\">&rarr;</span>"

#: inc/template-tags.php:48
msgid "Older posts"
msgstr "Articles plus anciens"

#: inc/jetpack.php:22
msgid "The featured content section displays on the front page above the first post in the content area."
msgstr "La partie de contenu présentée s'affiche sur la page d'accueil au-dessus du premier article dans la zone de contenu."

#: inc/template-tags.php:38
msgid "Post navigation"
msgstr "Navigation des articles"

#: inc/extras.php:61
msgid "Page %s"
msgstr "Page %s"

#: image.php:105
msgid "Both comments and trackbacks are currently closed."
msgstr "Commentaires et trackbacks clos."

#: image.php:103
msgid "Trackbacks are closed, but you can <a class=\"comment-link\" href=\"#respond\" title=\"Post a comment\">post a comment</a>."
msgstr "Trackbacks clos, mais vous pouvez <a class=\"comment-link\" href=\"#respond\" title=\"Poster un commentaire\">poster un commentaire</a>."

#: image.php:101
msgid "Comments are closed, but you can leave a trackback: <a class=\"trackback-link\" href=\"%s\" title=\"Trackback URL for your post\" rel=\"trackback\">Trackback URL</a>."
msgstr "Commentaires clos, mais vous pouvez laisser un trackback: <a class=\"trackback-link\" href=\"%s\" title=\"URL de Trackback\" rel=\"trackback\">URL de Trackback</a>."

#: image.php:99
msgid "<a class=\"comment-link\" href=\"#respond\" title=\"Post a comment\">Post a comment</a> or leave a trackback: <a class=\"trackback-link\" href=\"%s\" title=\"Trackback URL for your post\" rel=\"trackback\">Trackback URL</a>."
msgstr "<a class=\"comment-link\" href=\"#respond\" title=\"Poster un commentaire\">Poster un commentaire</a> ou faire un trackback: <a class=\"trackback-link\" href=\"%s\" title=\"URL de Trackback\" rel=\"trackback\">URL de Trackback</a>."

#: image.php:39
msgid "Next &rarr;"
msgstr "Suivant &rarr;"

#: image.php:23
msgid "Published <span class=\"entry-date\"><time class=\"entry-date\" datetime=\"%1$s\">%2$s</time></span> at <a href=\"%3$s\" title=\"Link to full-size image\">%4$s &times; %5$s</a> in <a href=\"%6$s\" title=\"Return to %7$s\" rel=\"gallery\">%8$s</a>"
msgstr "Publié <span class=\"entry-date\"><time class=\"entry-date\" datetime=\"%1$s\">%2$s</time></span> sur <a href=\"%3$s\" title=\"Lien vers image taille réelle\">%4$s &times; %5$s</a> dans <a href=\"%6$s\" title=\"Retour vers %7$s\" rel=\"gallery\">%8$s</a>"

#: image.php:38
msgid "&larr; Previous"
msgstr "&larr; Précédent"

#: header.php:41
msgid "Skip to content"
msgstr "Aller au contenu"

#. translators: If there are characters in your language that are not supported
#. by Raleway or Arvo, translate this to 'off'. Do not translate into your
#. own language.
#: functions.php:101
msgctxt "Web font: on or off"
msgid "on"
msgstr "on"

#: header.php:40
msgid "Menu"
msgstr "Menu"

#: functions.php:85
msgid "Sidebar"
msgstr "Colonne latérale"

#: functions.php:55
msgid "Primary Menu"
msgstr "Menu principal"

#: footer.php:19
msgid "Theme: %1$s by %2$s."
msgstr "Thème %1$s par %2$s."

#: footer.php:18
msgid "Proudly powered by %s"
msgstr "Fièrement propulsé par %s"

#: footer.php:18
msgid "A Semantic Personal Publishing Platform"
msgstr "Une plate-forme sémantique de publication personnelle"

#: content.php:52
msgid "Continue reading <span class=\"meta-nav\">&rarr;</span>"
msgstr "Continuer la lecture <span class=\"meta-nav\">&rarr;</span>"

#: content.php:20 content.php:39
msgid "% Comments"
msgstr "% commentaires"

#: content.php:20 content.php:39
msgid "0 comments"
msgstr "0 Commentaires"

#: content.php:20 content.php:39
msgid "1 Comment"
msgstr "Un commentaire"

#: content-page.php:22 content.php:28 image.php:34 image.php:107
#: inc/template-tags.php:77 inc/template-tags.php:100 single.php:103
msgid "Edit"
msgstr "Modifier"

#: content-page.php:16 content.php:53 image.php:93 single.php:63
msgid "Pages:"
msgstr "Pages&nbsp;:"

#: comments.php:59
msgid "Comments are closed."
msgstr "Les commentaires sont fermés."

#: comments.php:49
msgid "Newer Comments &rarr;"
msgstr "Commentaires plus récents &rarr;"

#: comments.php:48
msgid "&larr; Older Comments"
msgstr "&larr; Commentaire plus ancien"

#: comments.php:47
msgid "Comment navigation"
msgstr "Navigation des commentaires"

#: comments.php:28
msgid "One thought on &ldquo;%2$s&rdquo;"
msgid_plural "%1$s thoughts on &ldquo;%2$s&rdquo;"
msgstr[0] "Une réflexion au sujet de &laquo;&nbsp;%2$s&nbsp;&raquo;"
msgstr[1] "%1$s réflexions au sujet de &laquo;&nbsp;%2$s&nbsp;&raquo;"

#: archive.php:64
msgid "Archives"
msgstr "Archives"

#: archive.php:61
msgid "Links"
msgstr "Liens"

#: archive.php:58
msgid "Quotes"
msgstr "Citations"

#: archive.php:55
msgid "Videos"
msgstr "Vidéos"

#: archive.php:52
msgid "Images"
msgstr "Images"

#: archive.php:49
msgid "Asides"
msgstr "En passant"

#: archive.php:46
msgid "Yearly Archives: %s"
msgstr "Archives annuelles&nbsp;: %s"

#: archive.php:43
msgid "Monthly Archives: %s"
msgstr "Archives mensuelles&nbsp;: %s"

#: archive.php:40
msgid "Daily Archives: %s"
msgstr "Archives quotidiennes&nbsp;:"

#: archive.php:32
msgid "Author Archives: %s"
msgstr "Archives de l&rsquo;auteur&nbsp;: %s"

#: archive.php:25
msgid "Tag Archives: %s"
msgstr "Archives par mot-clé&nbsp;: %s"

#: archive.php:22
msgid "Category Archives: %s"
msgstr "Archives de catégorie&nbsp;: %s"

#. translators: %1$s: smiley
#: 404.php:44
msgid "Try looking in the monthly archives. %1$s"
msgstr "Essayez de voir du côté des archives mensuelles. %1$s"

#: 404.php:27
msgid "Most Used Categories"
msgstr "Catégories les plus utilisées"

#: 404.php:19
msgid "It looks like nothing was found at this location. Maybe try one of the links below or a search?"
msgstr "Contenu Introuvable. L'outil de Recherche ou les Liens ci-dessous vous remettront peut-être sur la voie&hellip;"

#: 404.php:15
msgid "Oops! That page can&rsquo;t be found."
msgstr "Oups&nbsp;! Cette page est introuvable."

#. Author URI of the plugin/theme
msgid "https://wordpress.com/themes/"
msgstr "https://wordpress.com/themes/"

#. Theme URI of the plugin/theme
msgid "https://wordpress.com/themes/untitled/"
msgstr "https://fr.wordpress.com/themes/untitled/"

#. Author of the plugin/theme
msgid "Automattic"
msgstr "Automattic"
